export interface Pokemon {
    id: number;
    name: string;
    url: string;
    abilities?: any[];
    baseExperience?: number;
    moves?: any[];
    sprites?: any[];
    stats?: any[];
    height?: number;
    weight?: number;
    types?: any[];
    collected: boolean;
  }
  