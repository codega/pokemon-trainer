import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PokemonResults } from 'src/app/models/pokemonResults.model';




@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private httpClient: HttpClient) { }
  

  getPokemon(apiCall:string): Observable<PokemonResults> {
    return this.httpClient.get<PokemonResults>(apiCall)
  
}
  getSinglePokemon(apiCall:string): Observable<any> {
    return this.httpClient.get<any>(apiCall)
  }
  
}
