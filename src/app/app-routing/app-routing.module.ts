import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartPageComponent } from '../pages/start-page/start-page.component'
import { CataloguePageComponent } from '../pages/catalogue-page/catalogue-page.component'
import { ProfilePageComponent } from '../pages/profile-page/profile-page.component'
import { DetailPageComponent } from '../pages/detail-page/detail-page.component'
import { AuthGuard } from '../auth/auth.guard';



const routes: Routes = [
  {
      path: '',
      component: StartPageComponent
  },
  {
      path: 'catalogue',
      component: CataloguePageComponent,
      canActivate:[AuthGuard]
  },
  {
    path: 'detail',
    component: DetailPageComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
    canActivate:[AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
  
  
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
