import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-detailed-poke-display',
  templateUrl: './detailed-poke-display.component.html',
  styleUrls: ['./detailed-poke-display.component.css']
})
export class DetailedPokeDisplayComponent implements OnInit {

  constructor(private localStorage: LocalStorageService) { 
  }

  public pokemon = this.localStorage.get('currentPokemon');
  public collected: boolean;

  isCollected(): void {
    const collection = this.localStorage.get('collectedPokemon');
    const pokeID = this.pokemon.id;
    if (collection.find( (pokemons: Pokemon) => {
      return pokemons.id === pokeID;
    })){
      this.collected = true;
    } else {
      this.collected = false;
    }
  }

  collectClicked(): void{
    const collection = this.localStorage.get('collectedPokemon');
    const pokeID = this.pokemon.id;
    if (collection.find( (pokemons: Pokemon) => {
      return pokemons.id === pokeID;
    })){
      alert('You already have this!');
    } else {
      collection.push(this.pokemon);
      alert('Collected! Well done!');
      this.collected = true;
    }
    this.localStorage.set('collectedPokemon', collection);
  }

  ngOnInit(): void {
    this.isCollected();
  }

}
