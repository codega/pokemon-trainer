import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-simple-poke-display',
  templateUrl: './simple-poke-display.component.html',
  styleUrls: ['./simple-poke-display.component.css']
})
export class SimplePokeDisplayComponent implements OnInit {
  @Input() pokemon:Pokemon;

  constructor(private router: Router, private dataService: DataService, private localStorage: LocalStorageService) { }
  
  pokemonClicked(){
    this.localStorage.set('currentPokemon', this.pokemon);
    this.router.navigateByUrl('/detail');
  };

  ngOnInit(): void {
    console.log(this.pokemon.name);
  }

}
