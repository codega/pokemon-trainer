import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginform: FormGroup = new FormGroup({
    username: new FormControl('', 
      [Validators.required, 
      Validators.minLength(5)])  
  });

  get username(){
    return this.loginform.get('username');
  }

  onLoginClicked(){
    if (this.loginform.valid){
      this.localStorageService.set('username', this.loginform.value.username);
      let startArray: any[] = []; 
      this.localStorageService.set('collectedPokemon', startArray);
      this.router.navigateByUrl('/catalogue');
    }
  }

  constructor(private router: Router, private localStorageService: LocalStorageService) { }




  ngOnInit(): void {
  }

}
