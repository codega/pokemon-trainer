import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { } 

  profileClicked(): void{
    this.router.navigateByUrl('/profile');
  }

  catalogueClicked(): void{
    this.router.navigateByUrl('/catalogue');
  }


  ngOnInit(): void {
  }

}
