import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { StartPageComponent } from './pages/start-page/start-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';


import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginFormComponent } from './components/forms/login-form/login-form.component';
import { SimplePokeDisplayComponent } from './components/functionality/simple-poke-display/simple-poke-display.component';
import { DetailedPokeDisplayComponent } from './components/functionality/detailed-poke-display/detailed-poke-display.component';
import { HeaderComponent } from './components/shared/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    ProfilePageComponent,
    CataloguePageComponent,
    DetailPageComponent,
    LoginFormComponent,
    SimplePokeDisplayComponent,
    DetailedPokeDisplayComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
