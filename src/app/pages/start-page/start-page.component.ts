import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {

  constructor(private router: Router, private localStorageService: LocalStorageService) { }

 

  ngOnInit(): void {
    if (this.localStorageService.get('username')){
      this.router.navigateByUrl('/catalogue');
    }
  }

}
