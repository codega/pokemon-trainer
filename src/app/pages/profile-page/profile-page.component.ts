import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  public collectedPokemon: any[] = [];
  public trainerName: string;

  constructor(private router: Router, private localStorage: LocalStorageService) { }

  logOut():void {
    this.localStorage.remove('allPokemons');
    this.localStorage.remove('username');
    this.localStorage.remove('currentPokemon');
    this.localStorage.remove('collectedPokemon');
    this.router.navigateByUrl('/');


  }

  ngOnInit(): void {
    this.trainerName = this.localStorage.get('username');
    this.collectedPokemon = this.localStorage.get('collectedPokemon');

  }

}
