import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { LocalStorageService } from '../../services/local-storage.service';


@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css']
})
export class CataloguePageComponent implements OnInit {

  constructor(private dataService: DataService, private localStorage: LocalStorageService) { }
  public pokemonList: Pokemon[] = [];
  public pokemonPage: Pokemon[] = [];
  private index: number = 0;
  private max: number = 20;

  updatePokemanPage(i:number, max:number) {
    this.pokemonPage = [];
    for (i; i < max; i++){
      const currentPokeObject = this.pokemonList[i];
      const pokeUrl = this.pokemonList[i].url;
      this.dataService.getSinglePokemon(pokeUrl).subscribe(val => {
        let abilities = [];
        let moves = [];
        let baseStats = [];
        let types = [];
        for (const ability of val.abilities){
          abilities.push(ability.ability.name);
        }
        for (const type of val.types){
          types.push(type.type.name);
        }
        for (const move of val.moves){
          moves.push(move.move.name);
        }
        for (const baseStat of val.stats){
          let newStat = {
            name: baseStat.stat.name,
            stat: baseStat.base_stat
          }
          baseStats.push(newStat);
        }
        currentPokeObject.baseExperience= val.base_experience;
        currentPokeObject.height=val.height;
        currentPokeObject.weight= val.weight;
        currentPokeObject.sprites= val.sprites.front_default;
        currentPokeObject.abilities= abilities;
        currentPokeObject.moves= moves;
        currentPokeObject.stats= baseStats;
        currentPokeObject.types= types;
        this.pokemonPage.push(currentPokeObject);
      });
    }
  };

  goForward(){
    //Edge cases
    if (this.index === 1020){
      this.index += 20;
      this.max += 10;
    } else if (this.index === 1040) {
      alert("Can't go any further!")
    } else {
      this.index += 20;
      this.max += 20;
    }
    this.updatePokemanPage(this.index, this.max);
  }

  goBackward(){
    //Edge cases
    if (this.index === 0){
      alert("Can't go back any further!")
    } else if (this.index === 10){
      this.index = 0;
      this.max = 20;
    } else {
      this.index -= 20;
      this.max -= 20;  
    }
    this.updatePokemanPage(this.index, this.max);
  }


  ngOnInit(): void {
    //Gets all pokemon
    if (!this.localStorage.get('allPokemons')){
      this.dataService.getPokemon('https://pokeapi.co/api/v2/pokemon?limit=1050/')
      .subscribe(val => {
        let id = 0;
        val.results.forEach(pokemon => {
          const nameCapitalized = pokemon.name.charAt(0).toUpperCase() + pokemon.name.slice(1)

          const pokemonObject = {
            id: id,
            name: nameCapitalized,
            url: pokemon.url,
            collected: false
          }
          id++;
          this.pokemonList.push(pokemonObject);
        });
        this.localStorage.set('allPokemons', this.pokemonList);    
        this.updatePokemanPage(this.index, this.max);
      });  
    } else {
      this.pokemonList = this.localStorage.get('allPokemons');
      this.updatePokemanPage(this.index, this.max);
    }
  }
}
